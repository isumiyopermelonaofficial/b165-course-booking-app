// [SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/courses.js');

// [SECTION] Routing Component
const route = exp.Router()

// [SECTION] [POST] Routes
	// Create a Single Course
route.post('/create', (req,res) => {
	//create a new course inside the DB.
	let data = req.body
	controller.createCourse(data).then(outcome => {
		res.send(outcome);
	});
});


// [SECTION] [GET] Routes - All routes for Get

	// Get All Courses
route.get('/all', (req,res) => {
	controller.getAllCourse().then(outcome => {
		res.send(outcome);
	})
});

	// Get Single Courses
route.get('/:id', (req,res) => {
	let courseId = req.params.id;
	controller.getCourse(courseId).then(result => {
		res.send(result);
	});
});

	//Get All active courses
route.get('/', (req,res) => {
	controller.getAllActiveCourse().then(result => {
		res.send(result);
	});
});

// [SECTION] [PUT] Routes
route.put('/:id', (req,res) => {
	let courseId = req.params.id;
	let details = req.body;

	let cName = details.name;
	let cDesc = details.description;
	let cCost = details.price;
	
	if (cName !== '' && cDesc !== '' && cCost !== '') {
		controller.updateCourse(courseId,details).then(result => {
			res.send(result);
		});
	} else {
		res.send('Incorrect Input, All inputs are required');
	}	
});

route.put('/:id/archive',(req,res) => {
	let courseId = req.params.id;
	controller.deactivateCourse(courseId).then(result => {
		res.send(result);
	});
});

route.put('/:id/reactivate', (req,res) => {
	let courseId = req.params.id;
	controller.reactivateCourse(courseId).then(result => {
		res.send(result);
	});
});

// [SECTION] [DEL] Routes
route.delete('/:id', (req,res) => {
	let courseId = req.params.id;
	controller.deleteCourse(courseId).then(result => {
		res.send(result);
	});
});


// [SECTION] Export Route System
module.exports = route;