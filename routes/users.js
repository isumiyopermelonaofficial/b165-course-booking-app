//[SECTION] Dependencies and Modules
const exp = require('express')
const controller = require('../controllers/users.js');
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;

//[SECTION] Routing Components
const route = exp.Router();

//[SECTION] Routes - [POST]
route.post('/register', (req,res) => {
	let data = req.body;
	controller.registerUser(data).then(result => {
		res.send(result);
	});

});

//Login User
route.post('/login', (req,res) => {
	let userDetails = req.body
	//console.table(data);

	controller.loginUser(userDetails).then(result => {
		res.send(`Access Token: ${result}`);
	});
});

// Enroll User
route.post('/enroll', verify, controller.enroll);


//[SECTION] Routes - [GET]

// Get All Users
route.get('/all', (req,res) => {
	controller.getAllUsers().then(result => {
		res.send(result);
	})
})

// Get Single Users
route.get('/:id', verify, (req,res) => {
	let userId = req.params.id;
	controller.getUser(userId).then(result => {
		res.send(result)
	});
});

//[SECTION] Routes - [PUT]
//[SECTION] Routes - [DELETE]
//[SECTION] Expose Route System
module.exports = route;
