//[SECTION] Packages and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

const courseRoutes = require('./routes/courses.js');
const userRoutes = require('./routes/users.js');

//[SECTION] Server
const app = express();
dotenv.config();

app.use(express.json());

const port = process.env.PORT;
const secret = process.env.CONNECTION_STRING;

// [SECTION] Application Routes
app.use('/courses',courseRoutes);
app.use('/users',userRoutes);

//[SECTION] Database Connection
mongoose.connect(secret);

	// Check conn status
	let connectionStatus = mongoose.connection;
	connectionStatus.on('open', () => console.log(`Database is connected`));


//[SECTION] Gateway Response
app.get('/', (req,res) => {
	res.send("Welcome to Isumiyo's - Course Booking App");
});

app.listen(port, () => console.log(`Server is running on port ${port}`));