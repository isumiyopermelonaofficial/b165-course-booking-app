// [SECTION] Dependencies and Modules
const Course = require('../models/Course.js');

// CREATE - Create a new course
module.exports.createCourse = (info) => {

let cName =	info.name;
let cDesc =	info.description;
let cCost =	info.price;
	
	let newCourse = new Course({
		name: cName,
		description: cDesc,
		price: cCost
	});

	return newCourse.save().then((savedCourse, error) => {
		if(error){
			return 'Failed to Save New Document'
		} else {
			return savedCourse;
		}
	});
};

// RETRIEVE
	// Retrieve all Courses
module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
			return result;
	});
};

	// Retrieve Single Course
module.exports.getCourse = (id) => {
	return Course.findById(id).then(result => {
		return result;
	});
};

module.exports.getAllActiveCourse = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};


// UPDATE
module.exports.updateCourse = (id,details) => {
	let cName = details.name;
	let cDesc = details.description;
	let cCost = details.price;
	
	let updatedCourse = {
		name: cName,
		description: cDesc,
		price: cCost
	}

	return Course.findByIdAndUpdate(id,updatedCourse).then((courseUpdated,err) => {
		if (err) {
			return 'Error: Unable to Update Course';
		} else {
			return 'Course Successfully Updated';
		}
	})
};

// Deactivate Course
module.exports.deactivateCourse = (id) => {
	
	let updates = {
		isActive: false
	}

	return Course.findByIdAndUpdate(id,updates).then((courseUpdated,err) => {
		if (err) {
			return 'Error: Unable to Archive Course';
		} else {
			return 'Course Saved to Archive';
		}
	})
};

// Reactivate Course
module.exports.reactivateCourse = (id) => {
	let updates = {
		isActive: true
	}
	return Course.findByIdAndUpdate(id,updates).then((courseActivted,err) => {
		if (err) {
			return 'Error: Unable to Reactivate Course';
		} else {
			return 'Course Successfully Reactivated';
		}
	})
}


// DELETE
module.exports.deleteCourse = (id) => {
	return Course.findByIdAndRemove(id).then((removedCourse,err) => {
		if (err) {
			return 'No Course Was Removed';
		} else {
			return `Course ${id} Deactivated`;
		}
	});
};