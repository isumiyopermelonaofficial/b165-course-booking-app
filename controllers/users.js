//[SECTION] Dependencies and Modules
const User = require('../models/User.js');
const Course = require('../models/Course.js');
const auth = require('../auth.js')
const bcrypt = require('bcrypt');
const dotenv = require("dotenv");
//const secret = "CourseBookingAPI";

const{verify, verifyAdmin} = auth;

dotenv.config();
const salt = Number(process.env.SALT);

//[SECTION]Functionalities [CREATE]
module.exports.registerUser = (info) => {
	let fName = info.firstName;
	let lName = info.lastName;
	let email = info.email;
	let passW = info.password;
	let gendr = info.gender;
	let mobil = info.mobileNo

	let newUser = new User({
		firstName: fName,
		lastName: lName,
		email: email,
		password: bcrypt.hashSync(passW,salt),
		gender: gendr,
		mobileNo: mobil
	});

	return newUser.save().then((user,rejected) => {
		if (user) {
			return user;
		} else {
			return 'Error: Unable to Register User';
		}
	});
};


module.exports.loginUser = (details) => {
	let userEmail = details.email;
	let userPass = details.password
	//let userPassword = details.password;

	return User.findOne({email:userEmail}).then(foundUser => {
		if (foundUser === null) {
			return 'Wrong Credentials'
		} else {
			const isPasswordCorrect = bcrypt.compareSync(userPass, foundUser.password);
			if(isPasswordCorrect){
				return auth.createAccessToken(foundUser);
			} else {
				return `Error: Incorrect Password`
			}
		}
	});
};

module.exports.enroll = async (req, res) => {
	console.log(req.user.id);
	console.log(req.body.courseId);

	if(req.user.isAdmin){
    return res.send("Action Forbidden")
  	} else {

  		let isUserUpdated = await User.findById(req.user.id).then(user => {

  			let newEnrollment = {
  				courseId: req.body.courseId
  			}
  			user.enrollments.push(newEnrollment);

  			return user.save().then(user => true).catch(err => err.message)

  		});

  		if(isUserUpdated !== true){
  			return res.send({message: isUserUpdated});
  		}

  		let isCourseUpdate = await Course.findById(req.body.courseId).then(course => {
  			let enrollee = {
  				userId: req.user.id
  			}
  			course.enrollees.push(enrollee);
  			return course.save().then(course => true).catch(err => err.message)
  		})
  		if(isCourseUpdate !== true){
  			return res.send({message: isCourseUpdate})
  		}
  		if(isUserUpdated && isCourseUpdate) {
  			return res.send({message: "Enrolled Succesfully."})
  		}
  	}	
};

//[SECTION]Functionalities [RETRIEVE]
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};

module.exports.getUser = (id) => {
	return User.findById(id).then(result => {
		return result;
	});
};


//[SECTION]Functionalities [UPDATE]
//[SECTION]Functionalities [DELETE]